import Algorithmia


def publish_algo(mgmt_api_key, api_address, algo_schema, algo_name, algo_hash):
    client = Algorithmia.client(api_key=mgmt_api_key, api_address=api_address)

    algo = client.algo("{}/{}".format(algo_name, algo_hash))
    results = algo.versions(1, published=True).results
    if len(results) > 0:
        cur_version = results[0]["version_info"]
        print("--- Last release version : {} ---".format(cur_version))
    else:
        print("--- Working with fresh project (no previous release found)")

    print("--- Attempting to release a new {} version".format(algo_schema))
    try:
        algo.publish(
            version_info={
                "version_type": algo_schema,
                "release_notes": "Automatically deployed by CI",
            },
            details={"label": "CICD"},
        )
        latest_version = algo.versions(1, published=True).results[0]["version_info"]
        print("--- New version {} successfully published".format(latest_version))
    except Algorithmia.errors.ApiError as e:
        if "Version already published" in str(e):
            print(e)
            print("--- Successfully ending algorithm publishing step.")
        else:
            print("---- Received an error from Algorithmia Data API: {}".format(e))
            raise e
    except Exception as e:
        print("---- Received an exception: {}".format(e))
        raise e
